#ifndef CONSOLEUTILS_H
#define CONSOLEUTILS_H
#include<iostream>

namespace consoleutils
{
namespace Color
{
enum Code {
    FG_BLACK = 30,
    FG_RED = 31,
    FG_GREEN = 32,
    FG_YELLOW = 33,
    FG_BLUE = 34,
    FG_MAGENTA = 35,
    FG_CYAN = 36,
    FG_WHITE = 37,
    FG_DEFAULT = 39,

    BG_BLACK = 40,
    BG_RED = 41,
    BG_GREEN = 42,
    BG_YELLOW = 43,
    BG_BLUE = 44,
    BG_MAGENTA = 45,
    BG_CYAN = 46,
    BG_WHITE = 47,
    BG_DEFAULT = 49
};



class Modifier {
  Code code;

public:
  Modifier(Code pcode) : code(pcode){}
friend std::ostream&
operator <<(std::ostream& os, const Modifier&mod) {
    return os << "\033[" << mod.code << "m";
}
};

    static Color::Modifier fg_red(Color::FG_RED);
    static Color::Modifier fg_def(Color::FG_DEFAULT);
    static Color::Modifier fg_blue(Color::FG_BLUE);
    static Color::Modifier fg_green(Color::FG_GREEN);
    static Color::Modifier fg_black(Color::FG_BLACK);
    static Color::Modifier fg_white(Color::FG_WHITE);
    static Color::Modifier fg_yellow(Color::FG_YELLOW);
    static Color::Modifier fg_magenta(Color::FG_MAGENTA);
    static Color::Modifier fg_cyan(Color::FG_CYAN);

static Color::Modifier getColorFromRGB(int red, int green, int blue)
{
   int sum = red + green + blue;
   int grey = sum / 3;

   if(red > blue && green > blue && red > 135 && green > 135) return fg_yellow;
   else if(red > green && blue > green && red > 135 && blue > 135) return fg_magenta;
   else if(green > red && blue > red && green > 135 && blue > 135) return fg_cyan;
   else if(red > green && red > blue && red > 115) return fg_red;
   else if(blue > red && blue > green && blue > 115) return fg_blue;
   else if(green > blue && green > red && green > 115) return fg_green;
   else if(grey < 50) return fg_black;
   else if(grey > 200) return fg_white;

   return fg_def;
}

static Color::Modifier getColorFromRGB(unsigned char *color)
{
    return getColorFromRGB(color[0], color[1], color[2]);
}


}

static const char CURSOR_UP='A';
static const char CURSOR_DOWN = 'B';
static const char CURSOR_FORWARD = 'C';
static const char CURSOR_BACKWARD = 'D';

static std::string moveCursor(char direction, int nbr = 1)
{
    std::string move = "\033[" + std::to_string(nbr) + direction;
    return move;
}




}

#endif // CONSOLEUTILS_H
