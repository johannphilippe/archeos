#ifndef STRINGUTILS_H
#define STRINGUTILS_H
#include<iostream>
#include<sstream>
#include<algorithm>

namespace stringutils {

static int count(char *c_str, char c)
{
    int i = -1;
    int cnt = 0;
    do
    {
        i++;
        if(c_str[i]==c) cnt++;
    } while(c_str[i] != '\0');

    return cnt;
}

static int count(std::string str, char c)
{
    int cnt = 0;
    for(int i =0; i<str.size(); i++) {
        if(str[i] == c) cnt++;
    }
    return cnt;
}

static int countLines(std::string s)
{
    return std::count(s.begin(), s.end(), '\n');
}


template<typename T> static T getNumber(const char *val,int precision=5) {
    std::stringstream ss(val);
    ss.precision(precision);
    T number;
    ss >> number;
    return number;
}

}


#endif // STRINGUTILS_H
