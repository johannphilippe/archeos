TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += link_pkgconfig

PKGCONFIG += opencv4

SOURCES += main.cpp

HEADERS += asciialgo.h \
    asciivideo.h \
    timer.h \
    lsignal.h \
    resources.h \
    archeos.h \
    curseswrapper.h \
    stringutils.h \
    consoleutils.h

LIBS += -lcurses -lpthread
