#ifndef TIMER
#define TIMER
#include<chrono>
#include<thread>
#include<atomic>
#include<mutex>
#include<condition_variable>
#include"lsignal.h"


class Timer
{
public:
    //ms -> interval in millisecond
    Timer(int ms) {
        duration=ms;
    }

    ~Timer() {
        keep_running=false;
        timer_thread.detach();
    }

    void start() {
        keep_running=true;
        timer_thread=std::thread(&Timer::run,this);
    }

    void run() {
        while(keep_running) {
            timer_sig();
            std::this_thread::sleep_for(std::chrono::milliseconds(duration));
            //run();
        }
    }

    void stop() {
        keep_running=false;
    }

    bool isRunning() {
        return keep_running ? true : false;
    }

    void setTime(int ms) {
        duration=ms;
    }

    lsignal::signal<void(void)> timer_sig;

protected:

private:
    std::atomic<bool> keep_running;
    std::atomic<int> duration;
    std::thread timer_thread;
};




#endif // TIMER

