#ifndef ASCIIALGO_H
#define ASCIIALGO_H

#include<iostream>
#include"opencv4/opencv2/opencv.hpp"


static int getGreyLevel(unsigned char *pixel)
{
    return ((pixel[0] + pixel[1] + pixel[2]) / 3);
}


//
static std::string asciiAlgo(int val, int algoIdx)
{
    //char res;
    std::string res;
    switch(algoIdx) {
    case 1: // simple algo
        if(val>=0 && val<20) {res=" ";}
        else if(val>=20 && val<60) {res=".";}
        else if(val>=60 && val <120) {res="|";}
        else if(val>=120 && val<180) {res="#";}
        else if(val>=180) {res="@";}
        break;
    case 2 : // more precise
        if(val<20) {res=" ";}
        else if(val>=20 && val<40) {res=".";}
        else if(val>=40 && val<60) {res=":";}
        else if(val>=60 && val < 80) {res=";";}
        else if(val>=80 && val <100) {res="!";}
        else if(val>=100 && val <120) {res="|";}
        else if(val>=120 && val<140) {res="=";}
        else if(val>=140 && val < 160) {res="%";}
        else if(val>=160 && val < 180) {res="H";}
        else if(val>=180 && val < 200) {res="$";}
        else if(val>=200 && val<220) {res="#";}
        else if(val>=220) {res="@";}
        break;
    case 3: // better than 1 (more clear)
        if(val>=0 && val<20) {                  res=" ";}
        else if(val>=20 && val<60) {         res=".";}
        else if(val>=60 && val <120) {      res=":";}
        else if(val>=120 && val<180) {     res="#";}
        else if(val>=180) {                         res="$";}
        break;
    case 4: // arbitrary
        if(val>=0 && val<20) {res="o";}
        else if(val>=20 && val<60) {res="u";}
        else if(val>=60 && val <120) {res="r";}
        else if(val>=120 && val<180) {res="$";}
        else if(val>=180) {res="@";}
        break;
    case 5: // algo with numbers and good precision // do one only with numbers
        if(val<20) {res="1";}
        else if(val>=20 && val<40) {res="2";}
        else if(val>=40 && val<60) {res="3";}
        else if(val>=60 && val < 80) {res="4";}
        else if(val>=80 && val <100) {res="5";}
        else if(val>=100 && val <120) {res="6";}
        else if(val>=120 && val<140) {res="7";}
        else if(val>=140 && val < 160) {res="8";}
        else if(val>=160 && val < 180) {res="9";}
        else if(val>=180 && val < 200) {res="$";}
        else if(val>=200 && val<220) {res="#";}
        else if(val>=220) {res="@";}
        break;
    case 6: //numbers
        if(val<30) {res="1";}
        else if(val>=20 && val<40) {res="2";}
        else if(val>=40 && val<60) {res="3";}
        else if(val>=60 && val < 90) {res="4";}
        else if(val>=90 && val <120) {res="5";}
        else if(val>=120 && val <150) {res="6";}
        else if(val>=150 && val<180) {res="7";}
        else if(val>=180 && val < 210) {res="8";}
        else if(val>=210) {res="9";}
        break;
    case 7: // big precision
        if(val<20) {res=" ";}
        else if(val>=20 && val<30) {res=      ".";}
        else if(val>=30 && val<40) {res=      ":";}
        else if(val>=40 && val < 60) {res=    ";";}
        else if(val>=60 && val <80) {res=     "!";}
        else if(val>=80 && val <100) {res=   "?";}
        else if(val>=100 && val<120) {res=  ")";}
        else if(val>=120 && val < 140) {res="|";}
        else if(val>=140 && val < 160) {res="$";}
        else if(val>=160 && val < 180) {res="%";}
        else if(val>=180 && val<200) {res=  "#";}
        else if(val>=200 && val < 220) {res="¥";}
        else if(val >=220) {res=                     "§";}
        break;
    case 8: //numbers
        if(val<30) {                                    res="A";}
        else if(val>=20 && val<40) {        res="U";}
        else if(val>=40 && val<60) {        res="O";}
        else if(val>=60 && val < 90) {      res="D";}
        else if(val>=90 && val <120) {     res="V";}
        else if(val>=120 && val <150) {   res="B";}
        else if(val>=150 && val<180) {    res="P";}
        else if(val>=180 && val < 210) {  res="R";}
        else if(val>=210) {                        res="Z";}
        break;
    case 9:

        if(val>=0 && val<20) {                  res="$";}
        else if(val>=20 && val<60) {         res="#";}
        else if(val>=60 && val <120) {      res=":";}
        else if(val>=120 && val<180) {     res=".";}
        else if(val>=180) {                         res=" ";}
        break;

    case 10:

        if(val>=0 && val<20) {                  res=" ";}
        else if(val>=20 && val<60) {         res=".";}
        else if(val>=60 && val <120) {      res=":";}
        else if(val>=120 && val<180) {     res="#";}
        else if(val>=180) {                         res="$";}
        break;

    case 11: // arbitrary with noise injection
        if(rand() % 3== 1) {
            int rnd = (rand() % 80) + 33;
            char c = rnd;
            res = c;
            return res;
        }

        if(val>=0 && val<20) {res="o";}
        else if(val>=20 && val<60) {res="u";}
        else if(val>=60 && val <120) {res="r";}
        else if(val>=120 && val<180) {res="$";}
        else if(val>=180) {res="@";}
        break;
    case 12:
        if(rand() % 5 == 1) {
            int rnd = (rand() % 80) + 33;
            char c = rnd;
            res = c;
            return res;
        }

        if(val<20) {res="~";}
        else if(val>=20 && val<40) {res="$";}
        else if(val>=40 && val<60) {res="*";}
        else if(val>=60 && val < 80) {res="§";}
        else if(val>=80 && val <100) {res="\"";}
        else if(val>=100 && val <120) {res="&";}
        else if(val>=120 && val<140) {res="<";}
        else if(val>=140 && val < 160) {res="%";}
        else if(val>=160 && val < 180) {res=";";}
        else if(val>=180 && val < 200) {res=",";}
        else if(val>=200 && val<220) {res="£";}
        else if(val>=220) {res="ç";}
        break;
    case 13:
        if(rand() % 3 == 1) {
            int rnd = (rand() % 80) + 33;
            char c = rnd;
            res = c;
            return res;
        }
        if(val<20) {res="~";}
        else if(val>=20 && val<40) {res="(";}
        else if(val>=40 && val<60) {res="*";}
        else if(val>=60 && val < 80) {res="§";}
        else if(val>=80 && val <100) {res=".";}
        else if(val>=100 && val <120) {res="-";}
        else if(val>=120 && val<140) {res="<";}
        else if(val>=140 && val < 160) {res="%";}
        else if(val>=160 && val < 180) {res=";";}
        else if(val>=180 && val < 200) {res=",";}
        else if(val>=200 && val<220) {res="à";}
        else if(val>=220) {res="ç";}
        break;

    case 14: // emoji unicode
        if(val>=0 && val<20) {res="😈";}
        else if(val>=20 && val<60) {res="😭";}
        else if(val>=60 && val <120) {res="😡";}
        else if(val>=120 && val<180) {res="😀";}
        else if(val>=180) {res="💀";}
        break;

    case 15: // precise emoji unicode
        if(val<20) {res="💥";}
        else if(val>=20 && val<40) {res="🖤";}
        else if(val>=40 && val<60) {res="😈";}
        else if(val>=60 && val < 80) {res="💣";}
        else if(val>=80 && val <100) {res="🥩";}
        else if(val>=100 && val <120) {res="💋";}
        else if(val>=120 && val<140) {res="👺";}
        else if(val>=140 && val < 160) {res="👹";}
        else if(val>=160 && val < 180) {res="🙈";}
        else if(val>=180 && val < 200) {res="🤖";}
        else if(val>=200 && val<220) {res="👽";}
        else if(val>=220) {res="👻";}
        break;

    case 16:
    {
        if(val == 0) {
            res = " ";
        } else {
            char r = (rand() % 74) + 48;
            res.push_back(r);
        }
        break;
    }
    case 17:
    {
        if(val >=100) {
            if(rand() % 3 == 1) { // noise
                int rnd = (rand() % 80) + 33;
                char c = rnd;
                res = c;
                return res;
            } else  {
                if(rand() % 2 == 1) {
                    res = "#";
                } else {
                    res = "@";
                }
            }

        } else {
            res = " ";
            return res;
            int rnd = rand() % 3;
            switch(rnd) {
            case 0:
                res = ".";
            case 1:
                res = ";";
            case 2:
                res = "¨";
            case 3:
                res = "`";
            }

        }

        break;
    }
    }
    return res;
}

/*
static cv::Mat rotateClockwise(cv::Mat mat) {
    cv::Mat res(mat.cols, mat.rows, CV_8UC3, cv::Scalar(0));

   for(int l = 0; l < mat.rows ; l++) {
       for(int c = 0; c < mat.cols ; c++) {
             cv::Vec3b color = mat.at<cv::Vec3b>(cv::Point(mat.rows - l,c));
             res.at<cv::Vec3b>(cv::Point(l,c)) = color;
       }
   }

    return res;
}

*/

#endif // ASCIIALGO_H
