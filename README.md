# Archeos

A tiny C++ software to make some ASCIIArt video contents with terminal.

To use it, simply open "archeos.h" in your favorite code editor, and fill the "fill_map()" function with paths to the videos you wish to degrade. 
Then build and run software. 
Once archeos starts, you can type to the terminal what you need : 
* list | l : will show you the list of videos
* show videoname : will display videoname in ASCIIART with relevant algorithms

With options : 
* -a : choose algorithm
* -xs -ys : resolution
* -c : color algorithm
