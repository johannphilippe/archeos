#ifndef ASCIIVIDEO_H
#define ASCIIVIDEO_H

#include<iostream>

#include"asciialgo.h"

#include"timer.h"
#include"consoleutils.h"
#include"stringutils.h"

using namespace stringutils;
using namespace consoleutils;
using namespace std;


struct VideoData
{

    VideoData(std::string n, int xs, int ys, int al, bool color_=false, int ms_ = 33) : name(n), xstep(xs), ystep(ys), algo(al), color(color_), ms(ms_)
    {

    }

    VideoData(VideoData &from) : name(from.name), xstep(from.xstep), ystep(from.ystep), algo(from.algo), color(from.color), ms(from.ms) {}

    VideoData &operator=(VideoData &from) {
        name = from.name;
        xstep = from.xstep;
        ystep = from.ystep;
        algo = from.algo;
        color = from.color;
        ms = from.ms;
    }

  std::string name;
  int xstep, ystep;
  int algo;
  bool color;
  int ms;

};



class AsciiVideo
{

public:
    AsciiVideo(std::string videopath, bool color = false)  : capture(videopath.c_str()), x_step(1), y_step(1), x_idx(0), y_idx(0), algo(1), offset(0),
        timer(33), down(false)
    {
        if(!color) timer.timer_sig.connect(this, &AsciiVideo::showNextFrame);
        else timer.timer_sig.connect(this, &AsciiVideo::showNextFrameColored);
    }

    AsciiVideo(VideoData *data ) : capture(data->name.c_str()), x_step(data->xstep), y_step(data->ystep), x_idx(0), y_idx(0), offset(0),
        algo(data->algo), timer(data->ms), down(false)
    {
        if(!data->color) timer.timer_sig.connect(this, &AsciiVideo::showNextFrame);
        else timer.timer_sig.connect(this, &AsciiVideo::showNextFrameColored);
    }


    void setOffset(int of) {offset=of;}

    ~AsciiVideo() {
    }


    void setAlgo(int algorithm) {algo = algorithm;}

    void reset() {
        x_idx = 0;
        y_idx = 0;
    }


    bool next() {
        return capture.read(matrix);

    }

    bool next(cv::Mat &mat) {
        return capture.read(mat);
    }

    void setStep(int x, int y) {
        x_step = x;
        y_step = y;
    }

    std::string getFrameAsString() {
        std::string res("");
        for(int l = 0; l < matrix.rows; l+= y_step) {
            for(int o=0;o<offset;o++) res += " ";
            for(int c = 0; c < matrix.row(l).cols; c+= x_step) {
                unsigned char *p = matrix.ptr(l,c);
                res += asciiAlgo( getGreyLevel(p), algo);
            }
            res += "\n";
        }
        return res;
    }

    void setFrameAsColoredString() {

    }

    bool isRunning() {return timer.isRunning();}

    void showNextFrame()
    {
        if(this->next()) {
            if(down) std::cout << moveCursor(CURSOR_UP, ceil((double)matrix.rows / y_step)) << flush;
            std::cout << this->getFrameAsString() << flush;

            down=true;

        } else {
            timer.stop();
        }
    }

    void showNextFrameColored()
    {
        if(this->next()) {
            if(down) std::cout << moveCursor(CURSOR_UP, ceil((double)matrix.rows / y_step)) << flush;
            for(int l = 0; l< matrix.rows;  l+= y_step)
            {
            for(int o=0;o<offset;o++) cout << " ";
                for(int c = 0; c<matrix.row(l).cols; c += x_step)
                {
                    unsigned char *p = matrix.ptr(l,c);
                    cout << Color::getColorFromRGB(p) << asciiAlgo(getGreyLevel(p),algo);

                }
                cout << endl;
            }
            down=true;


        } else {
            timer.stop();
        }
    }

    void start()
    {
        timer.start();
    }

    void stop()
    {
        timer.stop();
    }



private:
   Timer timer;
   cv::VideoCapture capture;
   cv::Mat matrix;
   int x_idx, y_idx;
   int x_step, y_step;
   int algo;
   bool down;

   int offset;

};

#endif // ASCIIVIDEO_H
