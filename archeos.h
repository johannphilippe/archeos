#ifndef NC_METHODS_H
#define NC_METHODS_H

#include"asciialgo.h"
#include"asciivideo.h"
#include"resources.h"
#include"timer.h"

#include"stringutils.h"
#include"consoleutils.h"

#include<unordered_map>

#include<regex>
#include<stack>

using namespace stringutils;
using namespace std;
using namespace consoleutils;

namespace archeos {
static int line = 0;
static int col = 0;
static int offset = 6;

std::stack<std::string> input_stack;

class LoadBar
{
public:
    LoadBar( int ms = 50, int step_=2, string loading = "Loading - ") : timer(ms), step(step_), cur(0), text(loading)
    {
        line +=1;

        timer.timer_sig.connect(this, &LoadBar::move);
        //timer.start();
    }
    ~LoadBar(){}

    void start(){
        timer.start();
    }

    void reset()
    {
        timer.stop();
        cur = 0;
        timer.start();
    }

    void stop()
    {
        timer.stop();
    }

    void reload(int ms = 50, int step_=2)
    {
        timer.stop();
        step = step_;
        cur = 0;
        timer.setTime(ms);
        timer.start();
    }

    std::string generateBar(int percent)
    {
        std::string res = text +  "[";
        for(int i=0; i<100; i+=2)
        {
            res += (i < percent) ? "#" : "-";
        }

        res += "]\r";
        return res;
    }

    void move()
    {
        cur += step;
        //mvprintw(line, col , generateBar(cur).c_str());
        //refresh();
        cout << generateBar(cur) << flush;

        if(cur >=100) {
            timer.stop();
            cout << endl;
        }
    }

    bool isRunning()
    {
        return timer.isRunning();
    }

private:
    Timer timer;
    int step, cur;
    std::string text;
};



struct objects
{
  LoadBar *bar;
  AsciiVideo *video;
};


static objects o;

static std::string getVideoPath() {return "/home/johann/Documents/video/videos";}
constexpr static const char *HOME_PATH = "/home/johann";

static std::unordered_map<std::string, VideoData *> video_map;
static void fill_map() {
    video_map["throne"] = new VideoData("/home/johann/Vidéos/throne.mp4", 3, 7, 10, false);
}


static std::string getProgression(int percent) {
    std::string res("[");
    for(int i=0; i<100;i+=2) {
        if(i <= percent) {
            res += "#";
        } else {
            res += "-";
        }

    }
    res += "]\n";
    return res;
}


static std::string userInput(std::string text = "Please enter an answer : ")
{
    cout << Color::fg_def << text;

    std::string inp;
    getline(cin, inp);
    input_stack.push(inp);

    return inp;

}



static void init()
{
    fill_map();
    std::cout << Color::fg_red << std::string(ARCHEOS_SW) << Color::fg_def << std::endl;
}


//get a Number from a char * (set precision)
template<typename T> static T getNumber(std::string val,int precision=5) {
    std::stringstream ss(val);
    ss.precision(precision);
    T number;
    ss >> number;
    return number;
}


#define ARGS_NOT_FOUND -1
static int getParamInt(std::string args, std::string match) {
    std::string full_match = match + " ([0-9]+)";
    std::regex reg(full_match);
    std::smatch sm;
    if(std::regex_search(args, sm, reg)) {
       return getNumber<int>(sm[1].str());
    }

    return ARGS_NOT_FOUND;
}

static VideoData *cur_vid;

static void parseInput(std::string input)
{
    if(input == "load terra")
    {
        o.bar = new LoadBar(100,2, "Loading Terra archives -- ");
        o.bar->start();
        while(o.bar->isRunning()) {}
    } else if(input.find("show") != input.npos)
    {
        delete o.video;
        cout << endl;

        std::regex reg("show\\s(.+)");
        std::smatch match;


        std::string vidname = input.substr(input.find("show") + 5);
        vidname = vidname.substr(0, vidname.find_first_of(' '));

        if(video_map.find(vidname) != video_map.end()) {

            // Map parameters if some
            int xs = getParamInt(input, "-x");
            int ys = getParamInt(input, "-y");
            int algo = getParamInt(input, "-a");
            int col = getParamInt(input, "-c");

            cur_vid = video_map[vidname];

            if(xs != ARGS_NOT_FOUND) cur_vid->xstep = xs;
            if(ys != ARGS_NOT_FOUND) cur_vid->ystep = ys;
            if(algo != ARGS_NOT_FOUND) cur_vid->algo = algo;
            if(col != ARGS_NOT_FOUND) cur_vid->color = (col == 1) ? true : false;


            o.video = new AsciiVideo(cur_vid);
            o.video->setOffset(6);
            o.video->start();
            while(o.video->isRunning() && getchar() != '\n') {
                //if(getchar() == 'n') {
                    o.video->stop();
                    cout <<  endl;
                //}
            }
        }

    } else if(input == "list" || input == "ls" || input == "l")
    {
        std::string ls("List of found archives : \n");
        for(auto & it : video_map)
        {
            std::string args =
                    std::to_string(it.second->xstep) + " " + std::to_string(it.second->ystep) + " - algo : "
                    + std::to_string(it.second->algo) + " - colode : " + ((it.second->color == true) ? std::string("TRUE") : std::string("FALSE"));
            ls += "- " + it.first + "\n";
            ls += args + "\n";
        }
        cout << Color::fg_def << ls << endl;
    }
}



}



#endif // NC_METHODS_H
